# DragDrop

# Description #
This is the Drag&Drop file used to control the robot PR2.
I have used the MVC : require.js, and a Drag&Drop Technology.

# How to run that ? #
You have to use the LastVersion (**DragDropV3**), this version is made to work **without Pyride**.

If you want to use it with Pyride, then use "D**ragDropPyrideVersion**" folder

You will need to download **Tornado**, and then launch the api with python : 
```
#!python

python apijson.py
```

Then open SinglePage.html in your browser.

# Screenshots #

![alt text](image/Interface.png "ScreenShot")